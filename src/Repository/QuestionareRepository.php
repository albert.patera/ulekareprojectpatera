<?php

namespace App\Repository;

use App\Entity\Questionare;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Questionare|null find($id, $lockMode = null, $lockVersion = null)
 * @method Questionare|null findOneBy(array $criteria, array $orderBy = null)
 * @method Questionare[]    findAll()
 * @method Questionare[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionareRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Questionare::class);
    }

    // /**
    //  * @return Questionare[] Returns an array of Questionare objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Questionare
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
