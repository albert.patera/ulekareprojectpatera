<?php
namespace App\Controller;


use App\Entity\Questionare;

use App\Repository\QuestionareRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TestType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Annotation\Route;
//load my own text type for better look
use App\Form\Type\UploadFileType;

class QuestionaireController extends AbstractController
{
   /**
    * @Route("/", name="upload_file")
    */
    public function index(Request $request)  : Response
    {
        
       $csvData = array();

        $em = $this->getDoctrine()->getManager();
        $question = $this->getDoctrine()->getRepository(Questionare::class)->findAll();
     //   dd($question);
        $form = $this->createFormBuilder()
        ->add('file', UploadFileType::class)
        ->getForm();

       $form->handleRequest($request);

        /**
         * There will be data from uploaded CSV file (parse data from csv file)
         * Maybe PHP 7.1 function or symfony libs 
         * There are testing data for now 
         */
       if($form->isSubmitted() && $form->isValid())
       {
            
            $item = new Questionare();
            $file = $form['file']->getData();
            $quest = 'Jake je dnes pocasi ? ';
            $response = 'Cloudy';
            $firtname = 'Jan';
            $lastName = 'Novak';
            $item->setFirstName($firtname);
            $item->setQuetion($quest);
            $item->setLastName($lastName);
            $item->setResponse($response);
            $item->setDate(new \Datetime());
            $em->persist($item);
            $em->flush();
            dump("successed");

           // dd($item);
           try {
            $tash = $em->getRepository(Questionare::class)->findByExampleField();
            //$resp = $em->getRepository(Questionare::class)->findAllRespondent();
            $count = $em->getRepository(Questionare::class)->countQuery();
            dd($count);
            //  dd('super');
            } catch(\Exception $e) {
                throw new \Exception($e->getMessage());
            }
       }
        return $this->render('homepage/index.html.twig', [
            'quest' => $question,
            'form' => $form->createView()
        ]);
    }
}